package com.alfinabalqis.ulanganharian;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private EditText panjang,lebar,tinggi;
    private double p,l,t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        panjang = findViewById(R.id.txtpanjang);
        lebar = findViewById(R.id.txtlebar);
        tinggi = findViewById(R.id.txttinggi);
    }

    private void data (List<String> variabel){
        String length = panjang.getText().toString();
        String width = lebar.getText().toString();
        String height = tinggi.getText().toString();

        p = Double.parseDouble(length);
        l = Double.parseDouble(width);
        t = Double.parseDouble(height);
    }

    public void keliling(View view) {
        if (panjang.getText().toString().isEmpty() && lebar.getText().toString().isEmpty() && tinggi.getText().toString().isEmpty()){
            panjang.setError("harap diisi dahulu");
            lebar.setError("harap diisi dahulu");
            tinggi.setError("harap diisi dahulu");
            return;
        }
        if (panjang.getText().toString().isEmpty() && lebar.getText().toString().isEmpty()){
            panjang.setError("harap diisi dahulu");
            lebar.setError("harap diisi dahulu");
            return;
        }
        if (panjang.getText().toString().isEmpty() && tinggi.getText().toString().isEmpty()){
            panjang.setError("harap diisi dahulu");
            tinggi.setError("harap diisi dahulu");
            return;
        }
        if (lebar.getText().toString().isEmpty() && tinggi.getText().toString().isEmpty()){
            lebar.setError("harap diisi dahulu");
            tinggi.setError("harap diisi dahulu");
            return;
        }
        if (panjang.getText().toString().isEmpty()){
            panjang.setError("harap diisi dahulu");
            return;
        }
        if (tinggi.getText().toString().isEmpty()){
            tinggi.setError("harap diisi dahulu");
            return;
        }
        if (lebar.getText().toString().isEmpty()){
            lebar.setError("harap diisi dahulu");
            return;
        }
        List<String> variabel = new ArrayList<>();
        data(variabel);

        Double keliling = 4*(p+l+t);

        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("hasil", String.valueOf("Keliling Balok adalah "));
        intent.putExtra("data", String.valueOf(keliling));

        startActivity(intent);
    }

    public void luas(View view) {
        if (panjang.getText().toString().isEmpty() && lebar.getText().toString().isEmpty() && tinggi.getText().toString().isEmpty()){
            panjang.setError("harap diisi dahulu");
            lebar.setError("harap diisi dahulu");
            tinggi.setError("harap diisi dahulu");
            return;
        }
        if (panjang.getText().toString().isEmpty() && lebar.getText().toString().isEmpty()){
            panjang.setError("harap diisi dahulu");
            lebar.setError("harap diisi dahulu");
            return;
        }
        if (panjang.getText().toString().isEmpty() && tinggi.getText().toString().isEmpty()){
            panjang.setError("harap diisi dahulu");
            tinggi.setError("harap diisi dahulu");
            return;
        }
        if (lebar.getText().toString().isEmpty() && tinggi.getText().toString().isEmpty()){
            lebar.setError("harap diisi dahulu");
            tinggi.setError("harap diisi dahulu");
            return;
        }
        if (panjang.getText().toString().isEmpty()){
            panjang.setError("harap diisi dahulu");
            return;
        }
        if (tinggi.getText().toString().isEmpty()){
            tinggi.setError("harap diisi dahulu");
            return;
        }
        if (lebar.getText().toString().isEmpty()){
            lebar.setError("harap diisi dahulu");
            return;
        }

        List<String> variabel = new ArrayList<>();
        data(variabel);

        Double luas = 2*(p*l+p*t+l*t);

        Intent intent = new Intent(this,ResultActivity.class);
        intent.putExtra("hasil", "Luas Permukaan Balok adalah ");
        intent.putExtra("data", String.valueOf(luas));

        startActivity(intent);
    }

    public void volume(View view) {
        if (panjang.getText().toString().isEmpty() && lebar.getText().toString().isEmpty() && tinggi.getText().toString().isEmpty()){
            panjang.setError("harap diisi dahulu");
            lebar.setError("harap diisi dahulu");
            tinggi.setError("harap diisi dahulu");
            return;
        }
        if (panjang.getText().toString().isEmpty() && lebar.getText().toString().isEmpty()){
            panjang.setError("harap diisi dahulu");
            lebar.setError("harap diisi dahulu");
            return;
        }
        if (panjang.getText().toString().isEmpty() && tinggi.getText().toString().isEmpty()){
            panjang.setError("harap diisi dahulu");
            tinggi.setError("harap diisi dahulu");
            return;
        }
        if (lebar.getText().toString().isEmpty() && tinggi.getText().toString().isEmpty()){
            lebar.setError("harap diisi dahulu");
            tinggi.setError("harap diisi dahulu");
            return;
        }
        if (panjang.getText().toString().isEmpty()){
            panjang.setError("harap diisi dahulu");
            return;
        }
        if (tinggi.getText().toString().isEmpty()){
            tinggi.setError("harap diisi dahulu");
            return;
        }
        if (lebar.getText().toString().isEmpty()){
            lebar.setError("harap diisi dahulu");
            return;
        }

        List<String> variabel = new ArrayList<>();
        data(variabel);

        Double volume = p*l*t;

        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("hasil", String.valueOf("Volume Balok adalah "));
        intent.putExtra("data", String.valueOf(volume));

        startActivity(intent);
    }
}
