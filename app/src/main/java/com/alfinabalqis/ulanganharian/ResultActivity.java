package com.alfinabalqis.ulanganharian;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {
    private TextView before, hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        before = findViewById(R.id.txtbefore);
        hasil = findViewById(R.id.txthasil);

        Intent intent = getIntent();

        String result = intent.getStringExtra("data");
        String x = intent.getStringExtra("hasil");
        before.setText(x);
        hasil.setText(result);
    }
}
